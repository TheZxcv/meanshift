#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import matplotlib.pyplot as plt
import numpy as np

from bindings import meanshift
from mpl_toolkits.mplot3d import Axes3D
from PIL import Image


def load_image(infilename) -> np.ndarray:
    img = Image.open(infilename)
    img.load()
    data = np.asarray(img, dtype=np.int32)
    return data


def save_image(npdata, outfilename):
    img = Image.fromarray(np.uint8(npdata))
    img.save(outfilename)


def add_indices_as_cols(matrix):
    mshape = matrix.shape[:2]
    ishape = list(mshape[:]) + [1]
    indices = np.indices(mshape)
    row_indices = indices[0].reshape(ishape)
    col_indices = indices[1].reshape(ishape)

    res = np.append(matrix, row_indices, axis=2)
    res = np.append(res, col_indices, axis=2)
    return res


def image_to_points(imagefile, includeIndices=False):
    pic = load_image(imagefile)
    print('Size: {}'.format(pic.shape))
    res = np.asarray(pic, dtype=np.float32)
    if includeIndices:
        res = add_indices_as_cols(res)
    newshape = (res.shape[0] * res.shape[1], res.shape[2])
    return res.reshape(newshape).astype(np.float32)


def clusterise_image(imagefile, centers):
    # TODO: speed this up
    image = load_image(imagefile)
    centers = [np.asarray(c, dtype=np.int32) for c in centers]
    centers_coords = len(centers[0])
    for row in range(len(image)):
        for col in range(len(image[row])):
            if centers_coords == len(image[row][col]):
                point = image[row][col]
            else:
                point = np.concatenate((image[row][col], [row, col]))
            minCenter = centers[0]
            best = np.linalg.norm(minCenter - point)
            for center in centers:
                # if euclidean_distance(minCenter, point) > euclidean_distance(center, point):
                if best > np.linalg.norm(center - point):
                    minCenter = center
                    best = np.linalg.norm(center - point)
            image[row][col] = minCenter[:image.shape[2]]

    print('done updating image!')
    plt.imshow(image, interpolation='nearest')
    plt.show()
    save_image(image, 'output.png')
    print('done saving image!')


def cluster_pixels(centers, datapoints):
    dim = len(datapoints[0])
    clusters = []
    for _ in range(len(centers)):
        lists = []
        for _ in range(dim):
            lists.append([])
        clusters.append(tuple(lists))

    for point in datapoints:
        minI = 0
        minCenter = centers[0]
        for i, center in enumerate(centers):
            if np.linalg.norm(minCenter - point) > np.linalg.norm(center - point):
                minCenter = center
                minI = i
        for d in range(dim):
            clusters[minI][d].append(point[d])
    return clusters


def plot_pixels_clusters(centers, clusters, radius):
    ax = plt.axes(projection='3d')
    for i, center in enumerate(centers):
        color = tuple(map(lambda x: x / 255, center))
        ax.scatter(*clusters[i], c=[color])
    for c in centers:
        # Sphere
        u = np.linspace(0, 2*np.pi, 10)
        v = np.linspace(0, 2*np.pi, 10)
        x = radius * np.outer(np.cos(u), np.sin(v)) + c[0]
        y = radius * np.outer(np.sin(u), np.sin(v)) + c[1]
        z = radius * np.outer(np.ones(np.size(u)), np.cos(v)) + c[2]
        ax.plot_surface(x, y, z, color='black', alpha=0.02)
        ax.scatter(*c, c='black')
    plt.show()


def show_clusters(centers, points, h):
    clusters = cluster_pixels(centers, points)
    plot_pixels_clusters(centers, clusters, h)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--bandwidth', type=int,
                        default=25, help='Bandwidth')
    parser.add_argument('-i', '--include-indices',
                        default=False,
                        action='store_true',
                        help='Include indices to the space')
    parser.add_argument('-plot',
                        default=False,
                        action='store_true',
                        help='Plot color space')
    parser.add_argument('image', help='Input image')
    args = parser.parse_args()

    points = image_to_points(args.image, includeIndices=args.include_indices)
    print('Bandwidth: {}'.format(args.bandwidth))
    centers = meanshift(points, args.bandwidth)
    print('#clusters: {}'.format(len(centers)))
    clusterise_image(args.image, centers)

    if args.plot:
        show_clusters(centers, points, args.bandwidth)


if __name__ == '__main__':
    main()
