#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def plot_points(points, title=''):
    xs = np.asarray([p[0] for p in points])
    ys = np.asarray([p[1] for p in points])

    plt.title(title)
    plt.plot(xs, ys)
    plt.show()


sum_rows_block_sizes = {
    32: [9938, 9922, 9958],
    64: [9445, 9450, 9489],
    128: [9451, 9439, 9459],
    256: [9460, 9478, 9471],
    512: [9491, 9508, 9494],
    1024: [9653, 9662, 9657],
}
points = [[k, sum(v) / len(v)] for k, v in sum_rows_block_sizes.items()]
plot_points(points, title='Sum rows')

points_scalar_prod = np.asarray(
    [[k, sum(v) / len(v)] for k, v in
     {
        32: [10354, 10286, 10279],
        64: [9503, 9495, 9490],
        128: [9440, 9428, 9420],
        256: [9784, 9793, 9786],
        512: [10471, 10484, 10471],
        1024: [11422, 11435, 11442],
    }.items()]
)
plot_points(points_scalar_prod, title='Scalar product')


df = pd.read_csv('data/italian-street-50-sum_rows.csv', ',')
df['time'] = (df['TIME_ms'] - df['TIME_ms'].min()) / \
    (df['TIME_ms'].max() - df['TIME_ms'].min())
df.describe()
ax = df.plot(x='BLOCK_SIZE', y='time', label='italian-street')
# plt.show()

df2 = pd.read_csv('data/KDDCUP04Bio-100-sum_rows.csv', ',')
df2['time'] = (df2['TIME_ms'] - df2['TIME_ms'].min()) / \
    (df2['TIME_ms'].max() - df2['TIME_ms'].min())
df2.describe()
df2.plot(x='BLOCK_SIZE', y='time', label='KDDCUP04Bio', ax=ax)
ax.set_xlabel('block size')
ax.set_ylabel('normalized execution time')
plt.savefig('/tmp/time.png', transparent=True)
plt.show()
