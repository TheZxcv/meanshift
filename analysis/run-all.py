#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import subprocess
import re

df = pd.read_csv('dataset.csv', ',')

df['#clusters'] = [0]*len(df)
df['time'] = [float('nan')]*len(df)

upper = len(df)
for irow in range(0, upper):
    row = df.iloc[irow]
    if row.Type == 'Image':
        executable = './meanshift'
    else:
        executable = './meanshift-inst'
    print("\r{0:.0%}".format(irow / upper), end='')
    process = subprocess.run(
        [
            executable,
            '-b', str(row.Bandwidth),
            '../instances/{}'.format(row.Instance)
        ], encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    if process.returncode == 0:
        output = process.stdout.strip()
        time_regex = 'Elapsed time: ([0-9]+)ms'
        clusters_regex = '#clusters: ([0-9]+)'
        matches = re.search(time_regex, output)
        if matches:
            df.loc[irow, 'time'] = float(matches.group(1))
        matches = re.search(clusters_regex, output)
        if matches:
            df.loc[irow, '#clusters'] = int(matches.group(1))

print()
print('done!')
df.to_csv('results.csv', index=False)
