==7057== Profiling application: ./release/meanshift-inst -b10000 instances/s1.txt
==7057== Profiling result:
==7057== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "GeForce GTX 950M (0)"
    Kernel: sum_rows(float*, float const *, unsigned long, unsigned long, float const *, unsigned long, float)
         72                 warp_execution_efficiency                 Warp Execution Efficiency      99.88%     100.00%      99.98%
         72                         branch_efficiency                         Branch Efficiency      99.94%     100.00%      99.99%
         72                         shared_efficiency                  Shared Memory Efficiency       0.00%       0.00%       0.00%
         72                            gld_efficiency             Global Memory Load Efficiency      82.51%      82.51%      82.51%
         72                            gst_efficiency            Global Memory Store Efficiency     100.00%     100.00%     100.00%
         72                        achieved_occupancy                        Achieved Occupancy    0.852870    0.861654    0.858160
    Kernel: scalar_prod(float*, float const *, float const *, unsigned long)
       2880                 warp_execution_efficiency                 Warp Execution Efficiency      98.67%      98.67%      98.67%
       2880                         branch_efficiency                         Branch Efficiency     100.00%     100.00%     100.00%
       2880                         shared_efficiency                  Shared Memory Efficiency      54.96%      54.96%      54.96%
       2880                            gld_efficiency             Global Memory Load Efficiency     100.00%     100.00%     100.00%
       2880                            gst_efficiency            Global Memory Store Efficiency      12.50%      12.50%      12.50%
       2880                        achieved_occupancy                        Achieved Occupancy    0.435451    0.461591    0.447270
    Kernel: reduce_sum_kernel(float*, float const *, unsigned long)
       1440                 warp_execution_efficiency                 Warp Execution Efficiency      99.84%      99.84%      99.84%
       1440                         branch_efficiency                         Branch Efficiency     100.00%     100.00%     100.00%
       1440                         shared_efficiency                  Shared Memory Efficiency      92.25%      92.25%      92.25%
       1440                            gld_efficiency             Global Memory Load Efficiency     100.00%     100.00%     100.00%
       1440                            gst_efficiency            Global Memory Store Efficiency      12.50%      12.50%      12.50%
       1440                        achieved_occupancy                        Achieved Occupancy    0.488708    0.490635    0.489853

