==2364== Profiling application: ./meanshift -b50 ../instances/italian-street.png
==2364== Profiling result:
==2364== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "GeForce GTX 950M (0)"
    Kernel: sum_rows(float*, float const *, unsigned long, unsigned long, float const *, unsigned long, float)
         24                 warp_execution_efficiency                 Warp Execution Efficiency      97.95%     100.00%      99.22%
         24                         branch_efficiency                         Branch Efficiency      99.00%     100.00%      99.62%
         24                        achieved_occupancy                        Achieved Occupancy    0.818338    0.865247    0.856462
    Kernel: scalar_prod(float*, float const *, float const *, unsigned long)
       1440                 warp_execution_efficiency                 Warp Execution Efficiency      98.73%      98.73%      98.73%
       1440                         branch_efficiency                         Branch Efficiency     100.00%     100.00%     100.00%
       1440                        achieved_occupancy                        Achieved Occupancy    0.961306    0.961541    0.961424
    Kernel: reduce_sum_kernel(float*, float const *, unsigned long)
        480                 warp_execution_efficiency                 Warp Execution Efficiency      99.84%      99.84%      99.84%
        480                         branch_efficiency                         Branch Efficiency     100.00%     100.00%     100.00%
        480                        achieved_occupancy                        Achieved Occupancy    0.967577    0.968228    0.967935

