#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import subprocess
import re


def run_all(kernel):
    if kernel == 'scalar_prod':
        arg_kernel = '-p'
    elif kernel == 'sum_rows':
        arg_kernel = '-s'
    else:
        exit(-1)
    df = pd.read_csv('dataset.csv', ',')
    results = []

    upper = len(df)
    for i, block_size in enumerate(range(32, 1024, 32)):
        for irow in range(0, upper):
            row = df.iloc[irow]
            if row.Type == 'Image':
                executable = './meanshift'
            else:
                executable = './meanshift-inst'
            print("\r{0:.0%}".format(
                (i * upper + irow) / ((1024/32)*upper)), end='')
            process = subprocess.run(
                [
                    executable,
                    '-b', str(row.Bandwidth),
                    arg_kernel, str(block_size),
                    '../instances/{}'.format(
                        row.Instance)
                ], encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
            if process.returncode == 0:
                output = process.stdout.strip()
                time = float('nan')
                nclusters = 0

                time_regex = 'Elapsed time: ([0-9]+)ms'
                matches = re.search(time_regex, output)
                if matches:
                    time = float(matches.group(1))

                clusters_regex = '#clusters: ([0-9]+)'
                matches = re.search(clusters_regex, output)
                if matches:
                    nclusters = int(matches.group(1))

                res = {}
                for key, value in row.items():
                    res[key] = value
                res['block_size'] = block_size
                res['#clusters'] = nclusters
                res['time'] = time
                results.append(res)

    print()
    print('done!')
    results = pd.DataFrame(results)
    results.to_csv('results.csv', index=False)
