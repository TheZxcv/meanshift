"""
MeanShift module
"""

import ctypes
import numpy as np
import os
import pkg_resources
from numpy.ctypeslib import ndpointer
import math

LIB_NAME = 'libmeanshift-shared.so'

# LIB_PATH = pkg_resources.resource_filename(
#    'meanshift', 'lib/{}'.format(LIB_NAME))
BASE_PATH = os.path.abspath(os.path.dirname(__file__))
LIB_PATH = os.path.join(BASE_PATH, 'lib/{}'.format(LIB_NAME))


def _load_library():
    return ctypes.CDLL(LIB_PATH)


LIBRARY = _load_library()


class MeanShiftConfig(ctypes.Structure):
    _fields_ = [
        ("ncols", ctypes.c_size_t),
        ("nrows", ctypes.c_size_t),
        ("lowerbound", ctypes.c_float),
        ("upperbound", ctypes.c_float),
        ("data", ctypes.POINTER(ctypes.c_float)),
        ("data_d", ctypes.POINTER(ctypes.c_float)),
        ("extra", ctypes.c_void_p),
        ("block_size_sum", ctypes.c_size_t),
        ("block_size_prod", ctypes.c_size_t),
    ]


class MeanShiftResult(ctypes.Structure):
    _fields_ = [
        ("len", ctypes.c_size_t),
        ("points", ctypes.POINTER(ctypes.c_float)),
    ]


# meanshift_t meanshift_init(double *points, size_t nrows, size_t ncols, double lowerbound, double upperbound);
_meanshift_init = LIBRARY.meanshift_init
_meanshift_init.restype = MeanShiftConfig
_meanshift_init.argtypes = [
    ndpointer(ctypes.c_float, flags="C_CONTIGUOUS"),
    ctypes.c_size_t,
    ctypes.c_size_t,
    ctypes.c_float,
    ctypes.c_float
]

# double *meanshift(meanshift_t, double h);
_meanshift = LIBRARY.meanshift
_meanshift.restype = MeanShiftResult
_meanshift.argtypes = [
    MeanShiftConfig,
    ctypes.c_float,
]

# void meanshift_free(meanshift_t);
_meanshift_free = LIBRARY.meanshift_free
_meanshift_free.restype = None
_meanshift_free.argtypes = [
    MeanShiftConfig,
]


class MeanShift:
    def __init__(self, points, h, datarange=(0, 255)):
        self._points = points
        self._data = points.T.reshape(-1)
        self._ncols, self._nrows = tuple(map(int, points.shape))
        self._lowerbound, self._upperbound = datarange
        self._window_radius = h

        self._config = _meanshift_init(
            self._data, self._nrows, self._ncols, self._lowerbound, self._upperbound)

    def cluster(self):
        # TODO: need to free result
        res = _meanshift(self._config, self._window_radius)
        a = np.ctypeslib.as_array(res.points, shape=(res.len, self._nrows))
        return a

    def cluster_slow(self):
        def gaussian_kernel(distance, bandwidth):
            if distance > bandwidth:
                return 0
            return (1/(bandwidth*math.sqrt(2*math.pi))) * np.exp(-0.5*((distance / bandwidth))**2)

        res = []
        for _ in range(20):
            x = np.random.uniform(
                self._lowerbound, self._upperbound, (self._nrows,))

            empty_n = False
            done = False
            iterations = 0
            while not done and iterations < 300:
                numerator = 0
                denominator = 0
                for p in self._points:
                    distance = np.linalg.norm(p - x)
                    weight = gaussian_kernel(distance, self._window_radius)
                    numerator += (weight * p)
                    denominator += weight

                if denominator != 0:
                    new_x = numerator / denominator
                    done = math.isclose(np.linalg.norm(
                        x - new_x), 0, abs_tol=1e-3) or (any(np.vectorize(math.isnan)(new_x)))
                    x = new_x
                else:
                    empty_n = True
                    done = True
                iterations += 1
            if not empty_n:
                res.append(x)

        return res


def meanshift(points, window_radius):
    XS = np.asarray(points)
    meanshift = MeanShift(XS, window_radius, datarange=(XS.min(), XS.max()))
    return meanshift.cluster()
