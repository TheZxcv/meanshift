from setuptools import setup, Distribution, find_packages


class BinaryDistribution(Distribution):
    def has_ext_modules(self):
        return True


setup(
    name='meanshift',
    version='0.1',
    license='MIT',
    description='MeanShift Library',
    packages=find_packages(exclude=['tests']),
    package_data={
            'meanshift': ['lib/libmeanshift-shared.so'],
    },
    include_package_data=True,
    distclass=BinaryDistribution,
    install_requires=[
        'numpy',
    ],
)
