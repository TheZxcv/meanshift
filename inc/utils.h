#ifndef UTILS_H
#define UTILS_H

#include <algorithm>
#include <cmath> // for M_PI
#include <iostream>

#define MAX_THREADS_PER_BLOCK (1024UL)

#if defined(__CUDACC__)
#define ERROR_CHECK(cuda_fn) gpuAssert((cuda_fn), __FILE__, __LINE__)
inline void gpuAssert(cudaError_t code, const char *file, int line,
                      bool abort = true) {
  if (code != cudaSuccess) {
    std::cerr << "GPUassert: " << cudaGetErrorString(code) << " " << file << " "
              << line << std::endl;
    if (abort)
      exit(code);
  }
}
#endif

static inline void __assert_true(const char *expression, const char *file,
                                 int line) {
  std::cerr << "Assertion '" << expression << "' failed, file '" << file
            << "' line '" << line << "'." << std::endl;
  std::exit(-1);
}

#define assert_true(EXPRESSION)                                                \
  ((EXPRESSION) ? (void)0 : __assert_true(#EXPRESSION, __FILE__, __LINE__))

inline static size_t div_ceil(size_t num, size_t den) {
  return (num % den) ? (num / den + 1) : (num / den);
}

/** Float comparison **/

constexpr static float default_tolerance = 1e-3;

static inline bool is_close_rel_with_tolerance(float expected, float actual,
                                               float tolerance) {
  return std::abs(expected - actual) <=
         tolerance * std::abs(std::max(expected, actual));
}

static inline bool is_close_rel(float expected, float actual) {
  return is_close_rel_with_tolerance(expected, actual, default_tolerance);
}

constexpr static float default_epsilon = 1e-3;

static inline bool is_close_abs_with_epsilon(float expected, float actual,
                                             float epsilon) {
  return std::abs(expected - actual) <= epsilon;
}

static inline bool is_close_abs(float expected, float actual) {
  return is_close_abs_with_epsilon(expected, actual, default_epsilon);
}

#endif