#ifndef IMAGE_H
#define IMAGE_H

typedef struct {
  float *pixels;
  int width;
  int height;
  int nchannels;
} float_image;

float_image load_image(const char *filename);
bool save_image(const char *filename, float_image image);
void dispose_image(float_image &image);

#endif