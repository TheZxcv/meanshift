#ifndef MEANSHIFT_INTERNAL_H
#define MEANSHIFT_INTERNAL_H

#define MAX_ITER_MEANSHIFT 300
#define NPOINTS_SHIFTED 20

#include "meanshift.h"

bool meanshift_point(float *x, size_t len, float *m, meanshift_t config,
                     float h);
bool meanshift_point_step(float *m, const float *x, size_t len,
                          meanshift_t config, float h);

void meanshift_start_timer(meanshift_t config);
void meanshift_end_timer(meanshift_t config);
unsigned long meanshift_elapsed_time(meanshift_t config);

#endif