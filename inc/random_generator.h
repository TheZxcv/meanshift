#ifndef RANDOM_GENERATOR_H
#define RANDOM_GENERATOR_H

#include <random>

namespace PRNG_holder {
extern std::default_random_engine generator;
}

#endif