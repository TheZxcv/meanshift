#ifndef NORM_H
#define NORM_H

#include <cstddef>

float euclidean_norm(const float *const vec, const size_t len);
float euclidean_distance(const float *const a, const float *const b,
                         const size_t len);

#endif