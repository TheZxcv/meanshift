#ifndef REDUCE_SUM_H
#define REDUCE_SUM_H

#include <cstdlib>
#include <cuda.h>

float reduce_sum(const float *const vector, size_t length, cudaMemoryType type);

#endif