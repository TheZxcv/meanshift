#ifndef SCALAR_PROD_H
#define SCALAR_PROD_H

#include <cstddef>

__global__ void scalar_prod(float *res, const float *const a,
                            const float *const b, size_t len);

#endif