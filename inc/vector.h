#ifndef MS_VECTOR_H
#define MS_VECTOR_H

#include <cstddef>

void print_vector(const float *const vector, size_t len);
bool contains_nan(const float *const vector, size_t len);

#endif