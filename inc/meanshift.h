#ifndef MEANSHIFT_H
#define MEANSHIFT_H

#include <cstddef>

typedef struct meanshift_internal_s meanshift_internal_t;

typedef struct meanshift_s {
  size_t ncols;
  size_t nrows;
  float lowerbound;
  float upperbound;
  const float *data;
  meanshift_internal_t *extra;
  size_t block_size_sum;
  size_t block_size_prod;
} meanshift_t;

typedef struct meanshift_res_s {
  size_t len;
  float *points;
} meanshift_res_t;

extern "C" {
meanshift_t meanshift_init(float *points, size_t nrows, size_t ncols,
                           float lowerbound, float upperbound);

void meanshift_free(meanshift_t);

meanshift_res_t meanshift(meanshift_t, float h);
void meanshift_free_solution(meanshift_res_t);
}

#endif
