\section{Schema risoluzione}

Lo schema dell'algoritmo di \emph{mean shift} prevede di calcolare $\vec{m}(\vec{x})$ per un dato punto $\vec{x}$
e aggiornare $\vec{x}$ con $\vec{x} + \vec{m}(\vec{x})$ finch\'e la norma di $\vec{m}(\vec{x})$
non \`e minore di una certa soglia di tolleranza vicina a $0$.
\medskip

Data una collezione di punti $Y$, consideriamo la matrice $M$ ottenuta disponendo i punti come colonne dalla matrice.

Ad esempio, dato $Y = \left\{\vec{y}_1, \vec{y}_2, \vec{y}_3, \ldots, \vec{y}_n\right\}$, la matrice $M$ risultante \`e la seguente:

\resizebox {\linewidth} {!} {
    \begin{tikzpicture}
        \node[fill=none,draw=none] (matrix)
        {
            \begin{minipage}{\linewidth}
                $$
                    M  = \begin{pmatrix}
                        y_{11} & y_{21} & y_{31} & \dots  & y_{n1} \\
                        y_{12} & y_{22} & y_{32} & \dots  & y_{n2} \\
                        \vdots & \vdots & \vdots & \ddots & \vdots \\
                        y_{1d} & y_{2d} & y_{3d} & \dots  & y_{nd}
                    \end{pmatrix}
                $$
            \end{minipage}
        };
        \filldraw[draw=none,fill=red,opacity=0.2,rounded corners] (-1.6,0.99) rectangle (-1.05,-1.05);
        \filldraw[draw=none,fill=blue,opacity=0.2,rounded corners] (-0.7,0.99) rectangle (-0.15,-1.05);
        \filldraw[draw=none,fill=green,opacity=0.2,rounded corners] (0.15,0.99) rectangle (0.75,-1.05);
        \filldraw[draw=none,fill=yellow,opacity=0.2,rounded corners] (1.9,0.99) rectangle (2.55,-1.05);
    \end{tikzpicture}
}

Ottenuta tale matrice $M$, il calcolo del vettore $\vec{m}(\vec{x})$ \`e suddiviso in 3 passi.
\medskip

Nel primo passo viene calcolata la matrice $M'$ tale che $M_{ji} = \frac{y_{ij} - x_j}{h}$, nel seguente modo:

$$
    M' = \frac{M - \vec{x} \cdot \underline{\mathbf{1}}^T}{h}
$$

Nel secondo passo viene calcolato il vettore $\vec{w}$ tale che $w_i = \norm{\frac{\vec{x} - \vec{y}_i}{h}}^{2}$, cio\`e il valore di
similarit\`a tra $\vec{x}$ e $\vec{y}_i$.
Utilizzando il prodotto di \emph{Hadamard}~\cite{million2007hadamard}, denotato con $\circ$,
e definendo la seguente estensione di $g$:
$$
    \hat{g}(\vec{x}) = \begin{pmatrix}
        g(x_{1}) \\
        g(x_{2}) \\
        \vdots   \\
        g(x_{d})
    \end{pmatrix}
$$

Possiamo calcolare $\vec{w}$ nella seguente forma compatta:
$$
    \vec{w} = \hat{g}(\underline{\mathbf{1}}^T (M' \circ M'))
$$

Ed infine dato $\vec{w}$ possiamo calcolare il \emph{mean shift} $\vec{m}(\vec{x})$:

$$
    \vec{m}(\vec{x}) = \frac{
        \sum_{i = 1}^{n} \vec{y}_i w_i
    }{
        \sum_{i = 1}^{n}
        w_i
    } - \vec{x}
    = \frac{M \vec{w}}{\sum_{i = 1}^{n}
        w_i} - \vec{x}
$$

In Alg.~\ref{alg:meanshift} \`e mostrato l'algoritmo \emph{mean shift}
per un dato vettore $\vec{x}$.

\begin{algorithm}
    \linespread{1.25}\selectfont
    \scriptsize
    \caption{Mean shift}\label{alg:meanshift}
    \begin{algorithmic}[1]
        \Function{Meanshift}{$M \in \Real^{d\times n}$, $\vec{x} \in \Real^d$, $h > 0$}
        \State{$\vec{x}_0 = \vec{x}$}
        \Repeat
        \State{$M' \gets \frac{M - \vec{x}_i \cdot \underline{\mathbf{1}}^T}{h}$}
        \State{$\vec{w} \gets \hat{g}(\underline{\mathbf{1}}^T (M' \circ M'))$}
        \State{$sum \gets \sum_{i = 1}^{n} w_i$}
        \State{$\vec{m} \gets \frac{M\vec{w}}{sum} - \vec{x}_i$}
        \State{$\vec{x}_{i+1} \gets \vec{x}_i + \vec{m}$}
        \Until{$\norm{\vec{m}} \approx 0$}
        \Return{$\vec{x}_k$}
        \EndFunction{}
    \end{algorithmic}
\end{algorithm}
