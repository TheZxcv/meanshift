#include "norm.h"

#include "test_utils.h"

#define VEC_LEN (4096)

int main() {
  float vector[VEC_LEN];

  for (int i = 0; i < VEC_LEN; i++) {
    vector[i] = i;
  }

  float actual = euclidean_norm(vector, VEC_LEN);
  assert_true(is_close_rel(actual, 151321.19587156322));

  return 0;
}
