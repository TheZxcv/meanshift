#include "reduce_sum.h"
#include "test_utils.h"

static void test_sum_up_to(int upper) {
  float expected = 0;
  float *values = new float[upper];
  for (int i = 0; i < upper; i++) {
    values[i] = i;
    expected += i;
  }
  float result = reduce_sum(values, upper, cudaMemoryTypeHost);
  assert_true(is_close_rel(expected, result));
  delete[] values;
}

int main() {
  int cases[] = {1, 5, 20, 100, 1000, 456379, 33554432};
  size_t len = sizeof(cases) / sizeof(int);
  for (size_t i = 0; i < len; i++) {
    test_sum_up_to(cases[i]);
  }
  return 0;
}
