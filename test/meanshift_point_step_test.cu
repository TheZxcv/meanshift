#include "meanshift_internal.h"
#include "test_utils.h"

#define NDIMENSIONS 3

float data[NDIMENSIONS * 4] = {
    1, 0, 1, 2, /* 0th row */
    1, 1, 0, 2, /* 1st row */
    1, 2, 3, 4, /* 2nd row */
};

int main() {
  float x[] = {2, 0, 1};
  float *m = new float[NDIMENSIONS];
  meanshift_t config = meanshift_init(data, NDIMENSIONS, 4, 0, 4);
  meanshift_point_step(m, x, 1, config, 5);

  assert_true(is_close_rel(m[0], -1.032884));
  assert_true(is_close_rel(m[1], 0.962031));
  assert_true(is_close_rel(m[2], 1.421771));

  meanshift_free(config);
  delete[] m;
  return 0;
}
