#include "meanshift.h"

#include "random_generator.h"
#include "test_utils.h"

#define NDIMENSIONS 2

/**
 * 3 blobs with centers
 * ( 1,  1)
 * (-1, -1)
 * ( 1, -1)
 **/
float data[NDIMENSIONS * 20] = {
    /* 0th row */
    1.00565032, -1.14987781, 0.69293207, 1.34807465, -0.74689668, 1.44660947,
    -1.0157117, 0.66902116, 1.38617076, 1.14476311, -1.20291416, 0.97828274,
    -1.25131454, 0.84811521, 0.92503221, 1.18807097, 1.15069537, -0.74237113,
    0.89501746, -0.96177646,
    /* 1th row*/
    0.46636866, -0.94761473, -1.06287987, 1.14286372, -0.81878854, 1.12668975,
    -0.92296258, 0.98546943, -0.83116168, 1.0796627, -0.84451743, 1.22065614,
    -0.9718187, -1.21896645, -0.78476988, 1.10276174, -1.12994629, -0.80718594,
    -0.86997319, -1.33756175};

bool contains_point_close_to(meanshift_res_t actual, float expected_x,
                             float expected_y) {
  for (size_t i = 0; i < NDIMENSIONS * actual.len; i += NDIMENSIONS) {
    if (is_close_abs_with_epsilon(actual.points[i], expected_x, 0.15) &&
        is_close_abs_with_epsilon(actual.points[i + 1], expected_y, 0.15)) {
      std::cout << expected_x << ", " << expected_y << " => "
                << actual.points[i] << ", " << actual.points[i + 1]
                << std::endl;
      return true;
    }
  }
  return false;
}

int main() {
  PRNG_holder::generator.seed(42);
  meanshift_t config = meanshift_init(data, NDIMENSIONS, 20, -2, 2);
  meanshift_res_t res = meanshift(config, 0.566096701437681);

  for (size_t i = 0; i < NDIMENSIONS * res.len; i += NDIMENSIONS) {
    std::cout << "# " << res.points[i] << ", " << res.points[i + 1]
              << std::endl;
  }

  assert_true(res.len == 3);
  assert_true(contains_point_close_to(res, 1, 1));
  assert_true(contains_point_close_to(res, -1, -1));
  assert_true(contains_point_close_to(res, 1, -1));

  meanshift_free_solution(res);
  meanshift_free(config);
  return 0;
}
