#include "scalar_prod.cuh"

#include "test_utils.h"
#include "utils.h"

#include <iostream>
#include <numeric>

#define VEC_LEN (4096L)

int main() {
  const size_t vec_size = VEC_LEN * sizeof(float);
  float A[VEC_LEN];
  float B[VEC_LEN];
  float expected = 0.0;

  for (int i = 0; i < VEC_LEN; i++) {
    A[i] = i;
    B[i] = VEC_LEN - i;
    expected += A[i] * B[i];
  }

  float *A_d, *B_d, *results_d;
  ERROR_CHECK(cudaMalloc(&A_d, vec_size));
  ERROR_CHECK(cudaMemcpy(A_d, A, vec_size, cudaMemcpyHostToDevice));
  ERROR_CHECK(cudaMalloc(&B_d, vec_size));
  ERROR_CHECK(cudaMemcpy(B_d, B, vec_size, cudaMemcpyHostToDevice));

  size_t block_size = MAX_THREADS_PER_BLOCK;
  size_t results_len = div_ceil(VEC_LEN, block_size);
  size_t smem_size = block_size * sizeof(float);
  ERROR_CHECK(cudaMalloc(&results_d, results_len * sizeof(float)));

  scalar_prod<<<results_len, block_size, smem_size>>>(results_d, A_d, B_d,
                                                      VEC_LEN);
  ERROR_CHECK(cudaPeekAtLastError());

  float *results = new float[results_len];
  ERROR_CHECK(cudaMemcpy(results, results_d, results_len * sizeof(float),
                         cudaMemcpyDeviceToHost));

  float result = std::accumulate(results, results + results_len, (float)0.0);
  delete[] results;

  ERROR_CHECK(cudaFree(results_d));
  ERROR_CHECK(cudaFree(B_d));
  ERROR_CHECK(cudaFree(A_d));
  ERROR_CHECK(cudaDeviceReset());

  assert_true(is_close_rel(result, expected));

  return 0;
}
