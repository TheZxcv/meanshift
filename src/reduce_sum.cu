#include "reduce_sum.h"

#include "utils.h"

#include <numeric>

static __global__ void
reduce_sum_kernel(float *output, const float *const vector, size_t size) {
  extern __shared__ float temp[];

  unsigned int tid = threadIdx.x;
  unsigned int idx = blockIdx.x * (blockDim.x * 2) + threadIdx.x;

  float sum = (idx < size) ? vector[idx] : 0;

  if (idx + blockDim.x < size)
    sum += vector[idx + blockDim.x];

  temp[tid] = sum;
  __syncthreads();

  // do reduction in shared mem
  for (unsigned int stride = blockDim.x / 2; stride > 0; stride >>= 1) {
    if (tid < stride) {
      sum += temp[tid + stride];
      temp[tid] = sum;
    }
    __syncthreads();
  }

  // write result for this block to global mem
  if (tid == 0)
    output[blockIdx.x] = sum;
}

float reduce_sum(const float *const vector, size_t length,
                 cudaMemoryType type) {
  size_t block_size = MAX_THREADS_PER_BLOCK;
  size_t grid_size = div_ceil(length, 2 * block_size);

  float *output_d;
  ERROR_CHECK(cudaMalloc(&output_d, grid_size * sizeof(float)));
  ERROR_CHECK(cudaMemset(output_d, 0, grid_size * sizeof(float)));

  if (type == cudaMemoryTypeHost) {
    float *input_d;
    ERROR_CHECK(cudaMalloc(&input_d, length * sizeof(float)));
    ERROR_CHECK(cudaMemcpy(input_d, vector, length * sizeof(float),
                           cudaMemcpyHostToDevice));
    reduce_sum_kernel<<<grid_size, block_size, block_size * sizeof(float)>>>(
        output_d, input_d, length);
    ERROR_CHECK(cudaFree(input_d));
  } else {
    reduce_sum_kernel<<<grid_size, block_size, block_size * sizeof(float)>>>(
        output_d, vector, length);
  }
  ERROR_CHECK(cudaPeekAtLastError());

  float *results = new float[grid_size];
  ERROR_CHECK(cudaMemcpy(results, output_d, grid_size * sizeof(float),
                         cudaMemcpyDeviceToHost));
  float sum = std::accumulate(results, results + grid_size, (float)0.0);

  delete[] results;
  ERROR_CHECK(cudaFree(output_d));

  return sum;
}