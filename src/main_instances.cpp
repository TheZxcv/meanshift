#include "meanshift.h"

#include "utils.h"
#include "vector.h"
#include <fstream>
#include <getopt.h>
#include <iomanip>
#include <iostream>
#include <limits>
#include <sstream>
#include <vector>

typedef struct {
  float *data;
  float min;
  float max;
  size_t ndim;
  size_t npoints;
} points_t;

static int run_clustering(const char *filename, float bandwidth,
                          size_t block_size_sum, size_t block_size_prod);
static void print_usage(const char *const program) {
  std::cout << "Usage: " << program
            << " [-b number] [-t threads per block] [FILE]" << std::endl
            << "Options and arguments:" << std::endl
            << " -h        prints this usage message" << std::endl
            << " -b NUMBER uses the argument as bandwidth (default: 1)"
            << std::endl
            << " -s NUMBER the number of threads per block for sum_rows "
               "(default: 192)"
            << std::endl
            << " -p NUMBER the number of threads per block for scalar_prod "
               "(default: 128)"
            << std::endl;
}

int main(int argc, char *argv[]) {
  int c;
  float bandwidth = 1;
  size_t block_size_sum = 192;
  size_t block_size_prod = 128;

  opterr = 0;
  errno = 0;
  while ((c = getopt(argc, argv, "b:hs:p:")) != -1) {
    switch (c) {
    case 'b':
      try {
        bandwidth = std::stof(optarg);
      } catch (std::exception &e) {
        std::cerr << "Option -b expected a number: " << e.what() << std::endl;
        return EXIT_FAILURE;
      }
      break;
    case 's':
      try {
        block_size_sum = std::stoull(optarg);
      } catch (std::exception &e) {
        std::cerr << "Option -s expected a number: " << e.what() << std::endl;
        return EXIT_FAILURE;
      }
      break;
    case 'p':
      try {
        block_size_prod = std::stoull(optarg);
      } catch (std::exception &e) {
        std::cerr << "Option -p expected a number: " << e.what() << std::endl;
        return EXIT_FAILURE;
      }
      break;
    case 'h':
      print_usage(argv[0]);
      return EXIT_SUCCESS;
    case '?':
      if (optopt == 'b') {
        std::cerr << "Option -" << (char)optopt << " requires a number."
                  << std::endl;
      } else {
        std::cerr << "Unknown option `-" << (char)optopt << "'." << std::endl;
      }
      print_usage(argv[0]);
      return EXIT_FAILURE;
    default:
      abort();
    }
  }

  if (optind == argc) {
    std::cerr << "No input provided." << std::endl;
    return EXIT_FAILURE;
  }

  return run_clustering(argv[optind], bandwidth, block_size_sum,
                        block_size_prod);
}

points_t load_instance(const char *filename) {
  bool first_line = true;
  points_t instance{};
  instance.min = std::numeric_limits<float>::max();
  instance.max = std::numeric_limits<float>::min();

  std::ifstream infile(filename, std::ifstream::in);

  if (infile.fail()) {
    std::cerr << "Failed to open instance file: '" << filename << "'"
              << std::endl;
    assert_true(false);
  }

  std::vector<std::vector<float>> points;
  std::string line;
  while (std::getline(infile, line)) {
    std::istringstream iss{line};

    float value;
    std::vector<float> point{};
    while (iss >> value) {
      point.push_back(value);

      if (value < instance.min) {
        instance.min = value;
      }

      if (value > instance.min) {
        instance.max = value;
      }
    }

    if (first_line) {
      instance.ndim = point.size();
      first_line = false;
    }

    assert_true(point.size() == instance.ndim);

    points.push_back(point);
  }

  instance.npoints = points.size();
  instance.data = new float[instance.npoints * instance.ndim];

  if (instance.npoints == 0) {
    std::cerr << "Empty instance!" << std::endl;
    assert_true(false);
  }

  for (size_t j = 0; j < instance.ndim; j++) {
    for (size_t i = 0; i < instance.npoints; i++) {
      instance.data[j * instance.npoints + i] = points[i][j];
    }
  }

  return instance;
}

static int run_clustering(const char *filename, float bandwidth,
                          size_t block_size_sum, size_t block_size_prod) {
  points_t instance = load_instance(filename);
  meanshift_t config =
      meanshift_init(instance.data, instance.ndim, instance.npoints,
                     instance.min, instance.max);
  config.block_size_sum = block_size_sum;
  config.block_size_prod = block_size_prod;

  meanshift_res_t res = meanshift(config, bandwidth);
  meanshift_free(config);
  std::cout << "#clusters: " << res.len << std::endl;

  for (size_t i = 0; i < res.len; i++) {
    std::cout << std::setw(3) << i << ": ";
    print_vector(&res.points[i * instance.ndim], instance.ndim);
  }

  meanshift_free_solution(res);
  return 0;
}
