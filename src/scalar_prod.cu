#include "scalar_prod.cuh"

__global__ void scalar_prod(float *res, const float *const a,
                            const float *const b, size_t len) {
  extern __shared__ float temp[];

  unsigned int tid = threadIdx.x;
  unsigned int idx = blockIdx.x * blockDim.x + threadIdx.x;

  float acc = 0;
  while (idx < len) {
    acc += a[idx] * b[idx];
    idx += blockDim.x * gridDim.x;
  }

  temp[tid] = acc;

  __syncthreads();

  int i = blockDim.x / 2;
  while (i != 0) {
    if (tid < i)
      temp[tid] += temp[tid + i];
    __syncthreads();
    i /= 2;
  }

  if (tid == 0)
    res[blockIdx.x] = temp[0];
}
