#include "vector.h"

#include <cmath>
#include <iomanip>
#include <iostream>

void print_vector(const float *const vector, size_t len) {
  std::cout << "[";
  for (size_t i = 0; i < len; i++)
    std::cout << " " << std::fixed << std::setprecision(2) << vector[i];
  std::cout << " ]" << std::endl;
}

bool contains_nan(const float *const vector, size_t len) {
  for (size_t i = 0; i < len; i++) {
    if (std::isnan(vector[i])) {
      return true;
    }
  }
  return false;
}