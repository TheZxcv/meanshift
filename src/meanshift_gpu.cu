#include "meanshift_internal.h"

#include "norm.h"
#include "random_generator.h"
#include "reduce_sum.h"
#include "scalar_prod.cuh"
#include "utils.h"

#include <algorithm>
#include <numeric>
#include <random>
#include <vector>

const int NSTREAMS = 8;
struct meanshift_internal_s {
  float *data_d;
  float *x_d;
  float *weights_d;
  float *mean_d;
  cudaStream_t streams[NSTREAMS];
  cudaEvent_t start;
  cudaEvent_t end;
};

meanshift_t meanshift_init(float *points, size_t nrows, size_t ncols,
                           float lowerbound, float upperbound) {
  meanshift_t config{};
  config.ncols = ncols;
  config.nrows = nrows;
  config.lowerbound = lowerbound;
  config.upperbound = upperbound;
  config.data = points;
  config.block_size_sum = 192;
  config.block_size_prod = 128;

  struct meanshift_internal_s *internal = new meanshift_internal_s{};
  config.extra = internal;

  internal->data_d = nullptr;
  internal->x_d = nullptr;
  internal->weights_d = nullptr;
  internal->mean_d = nullptr;

  size_t size = nrows * ncols * sizeof(float);
  ERROR_CHECK(cudaMalloc(&internal->data_d, size));
  ERROR_CHECK(
      cudaMemcpy(internal->data_d, points, size, cudaMemcpyHostToDevice));

  ERROR_CHECK(
      cudaMalloc(&internal->x_d, NPOINTS_SHIFTED * nrows * sizeof(float)));
  ERROR_CHECK(cudaMalloc(&internal->weights_d,
                         NPOINTS_SHIFTED * ncols * sizeof(float)));

  for (int i = 0; i < NSTREAMS; i++)
    ERROR_CHECK(cudaStreamCreate(&internal->streams[i]));

  ERROR_CHECK(cudaEventCreate(&internal->start));
  ERROR_CHECK(cudaEventCreate(&internal->end));

  return config;
}

void meanshift_free(meanshift_t config) {
  ERROR_CHECK(cudaFree(config.extra->weights_d));
  ERROR_CHECK(cudaFree(config.extra->x_d));
  ERROR_CHECK(cudaFree(config.extra->data_d));
  for (int i = 0; i < NSTREAMS; i++)
    ERROR_CHECK(cudaStreamDestroy(config.extra->streams[i]));
  ERROR_CHECK(cudaEventDestroy(config.extra->start));
  ERROR_CHECK(cudaEventDestroy(config.extra->end));

  delete config.extra;
}

static __device__ float normal_kernel_profile(float n, float bandwidth) {
  return 0.5 * exp(-n / 2);
}

static __global__ void sum_rows(float *weights, const float *const M,
                                const size_t nrows, const size_t ncols,
                                const float *const vecx, const size_t count,
                                float h) {
  size_t col = blockDim.x * blockIdx.x + threadIdx.x;

  float w = 0;
  size_t off = (col / ncols) * nrows;

  for (int i = 0; i < nrows; i++) {
    if (col < count * ncols) {
      float xji = M[i * ncols + (col % ncols)];
      xji = (xji - vecx[off + i]) / h;
      w += xji * xji;
    }
  }

  if (col < count * ncols) {
    weights[col] = normal_kernel_profile(w, h);
  }
}

static void scalar_products_start(size_t grid_size, size_t block_size,
                                  const float *const points, const size_t nrows,
                                  const size_t ncols,
                                  const float *const weights,
                                  float *reductions_d, cudaStream_t stream) {
  for (size_t i = 0; i < nrows; i++) {
    float *results_d = reductions_d + (i * grid_size);
    scalar_prod<<<grid_size, block_size, block_size * sizeof(float), stream>>>(
        results_d, weights, points + (i * ncols), ncols);
  }
}

static void scalar_products_end(size_t grid_size, float *results,
                                const size_t nrows, float *reductions_d,
                                cudaStream_t stream) {

  float *rest = new float[nrows * grid_size];
  ERROR_CHECK(cudaMemcpyAsync(rest, reductions_d,
                              nrows * grid_size * sizeof(float),
                              cudaMemcpyDeviceToHost, stream));
  cudaStreamSynchronize(stream);
  for (size_t i = 0; i < nrows; i++) {
    float *r = rest + (i * grid_size);
    results[i] = std::accumulate(r, r + grid_size, (float)0.0);
  }

  delete[] rest;
}

bool meanshift_point_step(float *m, const float *x, size_t npoints,
                          meanshift_t config, float h) {
  int ndim = config.nrows;
  int N = config.ncols;

  ERROR_CHECK(cudaMemcpy(config.extra->x_d, x, npoints * ndim * sizeof(float),
                         cudaMemcpyHostToDevice));

  dim3 block(std::min(npoints * config.ncols, config.block_size_sum));
  dim3 grid(div_ceil(npoints * config.ncols, block.x));
  sum_rows<<<grid, block>>>(config.extra->weights_d, config.extra->data_d,
                            config.nrows, config.ncols, config.extra->x_d,
                            npoints, h);
  ERROR_CHECK(cudaPeekAtLastError());

  float *reductions_d;
  block.x = config.block_size_prod;
  grid.x = div_ceil(config.ncols, block.x);
  ERROR_CHECK(cudaMalloc(&reductions_d,
                         npoints * config.nrows * grid.x * sizeof(float)));
  for (size_t ipoint = 0; ipoint < npoints; ipoint++) {
    scalar_products_start(grid.x, block.x, config.extra->data_d, config.nrows,
                          config.ncols, config.extra->weights_d + ipoint * N,
                          reductions_d + ipoint * config.nrows * grid.x,
                          config.extra->streams[ipoint % NSTREAMS]);
  }

  float *sums = new float[npoints];
  for (size_t ipoint = 0; ipoint < npoints; ipoint++) {
    float sum = reduce_sum(config.extra->weights_d + ipoint * N, N,
                           cudaMemoryTypeDevice);
    sums[ipoint] = sum;
  }

  for (size_t ipoint = 0; ipoint < npoints; ipoint++) {
    scalar_products_end(grid.x, m + ipoint * ndim, config.nrows,
                        reductions_d + ipoint * config.nrows * grid.x,
                        config.extra->streams[ipoint % NSTREAMS]);
    float sum = sums[ipoint];
    if (sum == 0) {
      for (int i = 0; i < ndim; i++) {
        m[ipoint * ndim + i] = 0;
      }
    } else {
      for (int index = 0; index < ndim; index++) {
        int i = ipoint * ndim + index;
        m[i] = (m[i] / sum) - x[i];
      }
    }
  }
  ERROR_CHECK(cudaFree(reductions_d));

  delete[] sums;

  return true;
}

void meanshift_start_timer(meanshift_t config) {
  ERROR_CHECK(cudaEventRecord(config.extra->start));
}

void meanshift_end_timer(meanshift_t config) {
  ERROR_CHECK(cudaEventRecord(config.extra->end));
}

unsigned long meanshift_elapsed_time(meanshift_t config) {
  ERROR_CHECK(cudaEventSynchronize(config.extra->end));
  float ms = 0;
  ERROR_CHECK(
      cudaEventElapsedTime(&ms, config.extra->start, config.extra->end));
  return (long)ms;
}
