#include "meanshift_internal.h"

#include "norm.h"
#include "random_generator.h"
#include "utils.h"
#include "vector.h"

#include <algorithm>
#include <cstring>
#include <iostream>
#include <numeric>
#include <random>
#include <vector>

bool meanshift_point(float *x, size_t npoints, float *m, meanshift_t config,
                     float h) {
  size_t ndim = config.nrows;

  float error;
  int iter = 0;
  do {
    if (!meanshift_point_step(m, x, npoints, config, h))
      return false;

    for (size_t i = 0; i < npoints * ndim; i++) {
      x[i] += m[i];
    }
    error = euclidean_norm(m, npoints * ndim);
    std::cout << "\rError: " << error << std::endl;
    iter++;
  } while (error > h && iter < MAX_ITER_MEANSHIFT);

  return true;
}

static void random_point(float *point, size_t len,
                         std::uniform_real_distribution<float> &distribution,
                         const float *points, size_t npoints) {
  size_t j = distribution(PRNG_holder::generator);
  for (size_t i = 0; i < len; i++)
    point[i] = points[i * npoints + j];
}

meanshift_res_t meanshift(meanshift_t config, float h) {
  meanshift_start_timer(config);

  int ndim = config.nrows;
  std::vector<float *> centers{};
  std::vector<float *> to_free{};

  std::uniform_real_distribution<float> distribution(0, config.ncols);
  float *m = new float[NPOINTS_SHIFTED * ndim];
  float *x = nullptr;
  for (int i = 0; i < 10; i++) {
    if (!x) {
      x = new float[NPOINTS_SHIFTED * ndim];
    }
    for (int j = 0; j < NPOINTS_SHIFTED; j++) {
      random_point(&x[j * ndim], ndim, distribution, config.data, config.ncols);
    }
    if (!meanshift_point(x, NPOINTS_SHIFTED, m, config, h))
      continue;

    bool is_used = false;
    for (int j = 0; j < NPOINTS_SHIFTED; j++) {
      float *point = &x[j * ndim];

      if (contains_nan(point, ndim))
        continue;

      bool is_duplicate = false;
      for (const auto &c : centers) {
        if (euclidean_distance(point, c, ndim) < h) {
          is_duplicate = true;
          break;
        }
      }

      if (!is_duplicate) {
        centers.push_back(point);
        is_used = true;
      }
    }

    if (is_used) {
      to_free.push_back(x);
      x = nullptr;
    }
  }

  float *res = new float[ndim * centers.size()];
  for (size_t i = 0; i < centers.size(); i++) {
    memcpy(&res[i * ndim], centers[i], ndim * sizeof(float));
  }

  for (const auto &point : to_free) {
    delete[] point;
  }
  delete[] m;

  meanshift_end_timer(config);
  std::cout << "Elapsed time: " << meanshift_elapsed_time(config) << "ms"
            << std::endl;

  meanshift_res_t mresult{};
  mresult.len = centers.size();
  mresult.points = res;
  return mresult;
}

void meanshift_free_solution(meanshift_res_t solution) {
  delete[] solution.points;
}
