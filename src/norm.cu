#include "norm.h"

#include "scalar_prod.cuh"
#include "utils.h"

#include <numeric>

#ifdef ENABLE_GPU_NORM
float euclidean_norm(const float *const vec, const size_t len) {
  float *vec_d, *results_d;
  const size_t vec_size = len * sizeof(float);
  ERROR_CHECK(cudaMalloc(&vec_d, vec_size));
  ERROR_CHECK(cudaMemcpy(vec_d, vec, vec_size, cudaMemcpyHostToDevice));

  size_t block_size = std::min(len, MAX_THREADS_PER_BLOCK);
  size_t results_len = div_deil(len, block_size);
  size_t smem_size = block_size * sizeof(float);
  ERROR_CHECK(cudaMalloc(&results_d, results_len * sizeof(float)));

  scalar_prod<<<results_len, block_size, smem_size>>>(results_d, vec_d, vec_d,
                                                      len);
  ERROR_CHECK(cudaPeekAtLastError());

  float *results = new float[results_len];
  ERROR_CHECK(cudaMemcpy(results, results_d, results_len * sizeof(float),
                         cudaMemcpyDeviceToHost));

  float result = std::accumulate(results, results + results_len, (float)0.0);

  delete[] results;
  ERROR_CHECK(cudaFree(results_d));
  ERROR_CHECK(cudaFree(vec_d));

  return std::sqrt(result);
}
#else
float euclidean_norm(const float *const a, const size_t len) {
  float res = 0;
  for (size_t i = 0; i < len; i++) {
    res += a[i] * a[i];
  }
  return std::sqrt(res);
}
#endif

float euclidean_distance(const float *const a, const float *const b,
                         const size_t len) {
  float res = 0;
  for (size_t i = 0; i < len; i++) {
    res += (a[i] - b[i]) * (a[i] - b[i]);
  }
  return std::sqrt(res);
}