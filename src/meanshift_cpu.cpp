#include "meanshift_internal.h"

#include "norm.h"
#include "random_generator.h"
#include "utils.h"

#include <algorithm>
#include <chrono>
#include <cstring>
#include <iostream>
#include <numeric>
#include <random>
#include <vector>

struct meanshift_internal_s {
  float *weights;
  std::chrono::high_resolution_clock::time_point start;
  std::chrono::high_resolution_clock::time_point end;
};

meanshift_t meanshift_init(float *points, size_t nrows, size_t ncols,
                           float lowerbound, float upperbound) {
  meanshift_t config{};
  config.ncols = ncols;
  config.nrows = nrows;
  config.lowerbound = lowerbound;
  config.upperbound = upperbound;
  config.data = points;
  config.block_size_sum = 0;
  config.block_size_prod = 0;

  struct meanshift_internal_s *internal = new meanshift_internal_s{};
  internal->weights = new float[NPOINTS_SHIFTED * ncols];

  config.extra = internal;

  return config;
}

void meanshift_free(meanshift_t config) {
  delete[] config.extra->weights;
  delete config.extra;
}

static float normal_kernel_profile(float n, float bandwidth) {
  return 0.5 * exp(-n / 2);
}

bool meanshift_point_step(float *m, const float *xs, size_t npoints,
                          meanshift_t config, float h) {
  float *sums = new float[npoints];
  for (size_t ipoint = 0; ipoint < npoints; ipoint++) {
    sums[ipoint] = 0.0;
    const float *x = &xs[ipoint * config.nrows];
    float *weights = &config.extra->weights[ipoint * config.ncols];

    for (size_t col = 0; col < config.ncols; col++) {
      float w = 0;
      for (size_t i = 0; i < config.nrows; i++) {
        float xji = config.data[i * config.ncols + col];
        xji = (xji - x[i]) / h;
        w += xji * xji;
      }
      weights[col] = normal_kernel_profile(w, h);
      sums[ipoint] += weights[col];
    }
  }

  for (size_t ipoint = 0; ipoint < npoints; ipoint++) {
    float *weights = &config.extra->weights[ipoint * config.ncols];
    for (size_t row = 0; row < config.nrows; row++) {
      float tmp = 0.0;
      for (size_t i = 0; i < config.ncols; i++) {
        tmp += config.data[row * config.ncols + i] * weights[i];
      }
      m[ipoint * config.nrows + row] = tmp;
    }
  }

  for (size_t i = 0; i < npoints; i++) {
    for (size_t j = 0; j < config.nrows; j++) {
      size_t index = i * config.nrows + j;
      if (sums[i] == 0)
        m[index] = 0;
      else
        m[index] = (m[index] / sums[i]) - xs[index];
    }
  }
  delete[] sums;

  return true;
}

void meanshift_start_timer(meanshift_t config) {
  config.extra->start = std::chrono::high_resolution_clock::now();
}

void meanshift_end_timer(meanshift_t config) {
  config.extra->end = std::chrono::high_resolution_clock::now();
}

unsigned long meanshift_elapsed_time(meanshift_t config) {
  return std::chrono::duration_cast<std::chrono::milliseconds>(
             config.extra->end - config.extra->start)
      .count();
}