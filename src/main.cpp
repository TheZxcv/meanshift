#include "meanshift.h"

#include "image/image.h"
#include <getopt.h>
#include <iostream>
#include <limits>

static int run_clustering(const char *filename, float bandwidth,
                          size_t block_size_sum, size_t block_size_prod);
static void print_usage(const char *const program) {
  std::cout << "Usage: " << program
            << " [-b number] [-s threads per block for sum_rows]"
            << " [-p threads per block for scalar_prod] [FILE]" << std::endl
            << "Options and arguments:" << std::endl
            << " -h        prints this usage message" << std::endl
            << " -b NUMBER uses the argument as bandwidth (default: 25)"
            << std::endl
            << " -s NUMBER the number of threads per block for sum_rows "
               "(default: 192)"
            << std::endl
            << " -p NUMBER the number of threads per block for scalar_prod "
               "(default: 128)"
            << std::endl;
}

int main(int argc, char *argv[]) {
  int c;
  float bandwidth = 25;
  size_t block_size_sum = 192;
  size_t block_size_prod = 128;

  opterr = 0;
  errno = 0;
  while ((c = getopt(argc, argv, "b:hs:p:")) != -1) {
    switch (c) {
    case 'b':
      try {
        bandwidth = std::stof(optarg);
      } catch (std::exception &e) {
        std::cerr << "Option -b expected a number: " << e.what() << std::endl;
        return EXIT_FAILURE;
      }
      break;
    case 's':
      try {
        block_size_sum = std::stoull(optarg);
      } catch (std::exception &e) {
        std::cerr << "Option -s expected a number: " << e.what() << std::endl;
        return EXIT_FAILURE;
      }
      break;
    case 'p':
      try {
        block_size_prod = std::stoull(optarg);
      } catch (std::exception &e) {
        std::cerr << "Option -p expected a number: " << e.what() << std::endl;
        return EXIT_FAILURE;
      }
      break;
    case 'h':
      print_usage(argv[0]);
      return EXIT_SUCCESS;
    case '?':
      if (optopt == 'b') {
        std::cerr << "Option -" << (char)optopt << " requires a number."
                  << std::endl;
      } else {
        std::cerr << "Unknown option `-" << (char)optopt << "'." << std::endl;
      }
      print_usage(argv[0]);
      return EXIT_FAILURE;
    default:
      abort();
    }
  }

  if (optind == argc) {
    std::cerr << "No input provided." << std::endl;
    return EXIT_FAILURE;
  }

  return run_clustering(argv[optind], bandwidth, block_size_sum,
                        block_size_prod);
}

static int run_clustering(const char *filename, float bandwidth,
                          size_t block_size_sum, size_t block_size_prod) {
  float_image img = load_image(filename);
  meanshift_t config =
      meanshift_init(img.pixels, img.nchannels, img.height * img.width, 0, 255);
  config.block_size_sum = block_size_sum;
  config.block_size_prod = block_size_prod;

  meanshift_res_t res = meanshift(config, bandwidth);
  meanshift_free(config);
  std::cout << "#clusters: " << res.len << std::endl;

  float *point = new float[img.nchannels];
  for (int i = 0; i < img.height * img.width; i++) {
    for (int j = 0; j < img.nchannels; j++) {
      point[j] = img.pixels[img.height * img.width * j + i];
    }

    size_t min_point = -1;
    float min_dist = std::numeric_limits<float>::max();
    for (size_t j = 0; j < res.len; j++) {
      float dist = 0.0;
      for (int k = 0; k < img.nchannels; k++) {
        float val = point[k] - res.points[j * img.nchannels + k];
        dist += val * val;
      }
      if (dist < min_dist) {
        min_dist = dist;
        min_point = j;
      }
    }

    for (int j = 0; j < img.nchannels; j++) {
      img.pixels[img.height * img.width * j + i] =
          res.points[min_point * img.nchannels + j];
    }
  }

  if (!save_image("/tmp/out.png", img)) {
    std::cerr << "Failed to save output picture." << std::endl;
  }

  dispose_image(img);
  meanshift_free_solution(res);
  return 0;
}
