#include "image/image.h"

#include "utils.h"

#define PNG_DEBUG 3
#include <png.h>

typedef struct {
  int width;
  int height;
  png_byte color_type;
  png_byte bit_depth;
  png_bytep *row_pointers;
  int nchannels;
} raw_image;

raw_image read_png_file(const char *filename) {
  raw_image image{};

  /* open file and test for it being a png */
  FILE *fp = fopen(filename, "rb");
  assert_true(fp);
  /* 8 is the maximum size that can be checked */
  unsigned char header[8];
  fread(header, 1, 8, fp);
  assert_true(png_check_sig(header, 8));

  /* initialize stuff */
  png_structp png_ptr =
      png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  assert_true(png_ptr);

  png_infop info_ptr = png_create_info_struct(png_ptr);
  assert_true(info_ptr);

  if (setjmp(png_jmpbuf(png_ptr))) {
    assert_true(false);
  }

  png_init_io(png_ptr, fp);

  /* tell libpng how many bytes were used to check the signature */
  png_set_sig_bytes(png_ptr, 8);

  /* read header */
  png_read_info(png_ptr, info_ptr);

  image.width = png_get_image_width(png_ptr, info_ptr);
  image.height = png_get_image_height(png_ptr, info_ptr);
  image.color_type = png_get_color_type(png_ptr, info_ptr);
  image.bit_depth = png_get_bit_depth(png_ptr, info_ptr);
  assert_true(image.bit_depth == 8);

  /* read file */
  if (setjmp(png_jmpbuf(png_ptr))) {
    assert_true(false);
  }

  image.row_pointers = new png_bytep[image.height];
  image.nchannels = png_get_rowbytes(png_ptr, info_ptr) / image.width;
  for (int y = 0; y < image.height; y++) {
    image.row_pointers[y] = new png_byte[image.nchannels * image.width];
  }

  png_read_image(png_ptr, image.row_pointers);

  fclose(fp);
  /* dispose helper structs */
  png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);

  return image;
}

void dispose_raw_image(raw_image &image) {
  for (int y = 0; y < image.height; y++) {
    delete[] image.row_pointers[y];
    image.row_pointers[y] = NULL;
  }
  delete[] image.row_pointers;
  image.row_pointers = NULL;
}

void dispose_image(float_image &image) {
  delete[] image.pixels;
  image.pixels = NULL;
}

float_image load_image(const char *filename) {
  float_image image;
  raw_image raw = read_png_file(filename);

  image.height = raw.height;
  image.width = raw.width;
  image.nchannels = raw.nchannels;
  image.pixels = new float[image.height * image.width * image.nchannels];

  for (int y = 0; y < raw.height; y++) {
    for (int x = 0; x < raw.width; x++) {
      for (int z = 0; z < raw.nchannels; z++) {
        png_byte byte = raw.row_pointers[y][x * raw.nchannels + z];
        int iplane = z * raw.width * raw.height;
        int irow = y * raw.width;
        image.pixels[iplane + irow + x] = byte;
      }
    }
  }

  dispose_raw_image(raw);
  return image;
}

void write_png_file(const char *filename, raw_image &image) {
  /* create file */
  FILE *fp = fopen(filename, "wb");
  assert_true(fp);
  /***************/

  /* initialize stuff */
  png_structp png_ptr =
      png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  assert_true(png_ptr);

  png_infop info_ptr = png_create_info_struct(png_ptr);
  assert_true(info_ptr);

  if (setjmp(png_jmpbuf(png_ptr))) {
    assert_true(false);
  }

  png_init_io(png_ptr, fp);
  /********************/

  /* write header */
  if (setjmp(png_jmpbuf(png_ptr))) {
    assert_true(false);
  }

  png_set_IHDR(png_ptr, info_ptr, image.width, image.height, image.bit_depth,
               image.color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE,
               PNG_FILTER_TYPE_BASE);

  png_write_info(png_ptr, info_ptr);
  /****************/

  /* write bytes */
  if (setjmp(png_jmpbuf(png_ptr))) {
    assert_true(false);
  }

  png_write_image(png_ptr, image.row_pointers);
  /***************/

  /* end write */
  if (setjmp(png_jmpbuf(png_ptr))) {
    assert_true(false);
  }

  png_write_end(png_ptr, NULL);
  /*************/

  fclose(fp);

  /* dispose helper structs */
  png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
}

bool save_image(const char *filename, float_image image) {
  raw_image raw;
  raw.height = image.height;
  raw.width = image.width;
  raw.bit_depth = 8;
  raw.nchannels = image.nchannels;
  raw.color_type =
      image.nchannels == 3 ? PNG_COLOR_TYPE_RGB : PNG_COLOR_TYPE_RGBA;

  raw.row_pointers = new png_bytep[raw.height];
  for (int y = 0; y < raw.height; y++) {
    raw.row_pointers[y] = new png_byte[raw.nchannels * raw.width];
  }

  for (int y = 0; y < raw.height; y++) {
    for (int x = 0; x < raw.width; x++) {
      for (int z = 0; z < raw.nchannels; z++) {
        int iplane = z * raw.width * raw.height;
        int irow = y * raw.width;
        png_byte byte = image.pixels[iplane + irow + x];
        raw.row_pointers[y][x * raw.nchannels + z] = byte;
      }
    }
  }
  write_png_file(filename, raw);
  dispose_raw_image(raw);
  return true;
}