#!/bin/sh

nvprof --metrics warp_execution_efficiency,branch_efficiency,shared_efficiency,gld_efficiency,gst_efficiency,achieved_occupancy $1
