#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import matplotlib.pyplot as plt
import numpy as np

from bindings import meanshift
from itertools import cycle
from matplotlib import cm
from sklearn.cluster import estimate_bandwidth
from sklearn.datasets.samples_generator import make_blobs


def cluster_data(centers, datapoints):
    dim = len(datapoints[0])
    clusters = []
    for _ in range(len(centers)):
        lists = []
        for _ in range(dim):
            lists.append([])
        clusters.append(tuple(lists))

    for point in datapoints:
        minI = 0
        minCenter = centers[0]
        for i, center in enumerate(centers):
            if np.linalg.norm(minCenter - point) > np.linalg.norm(center - point):
                minCenter = center
                minI = i
        for d in range(dim):
            clusters[minI][d].append(point[d])
    return clusters


def plot_clusters(centers, clusters, radius):
    cycol = cycle('bgrcmyk')
    ax = plt.axes()
    for i, center in enumerate(centers):
        color = next(cycol)
        ax.scatter(*clusters[i], c=color)
    for c in centers:
        ax.add_artist(plt.Circle(c, radius, color='black', fill=False))
        ax.scatter(*c, c='black')
    plt.show()


def show_clusters(centers, points, h):
    clusters = cluster_data(centers, points)
    plot_clusters(centers, clusters, h)


def load_instance(filename):
    points = []
    with open(filename, 'r') as f:
        for line in f:
            points.append(list(map(float, line.split())))
    ndim = len(points[0])

    return np.asarray(points).astype(np.float32).reshape((-1, ndim))


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--bandwidth', type=float, help='Bandwidth')
    parser.add_argument('-n', '--sample-size', type=int, default=20000,
                        help='Sample size')
    parser.add_argument('-std', type=float, default=0.4,
                        help='Cluster standard deviation')
    parser.add_argument('instance', nargs='?', default=None,
                        help='Input instance, a random instance is created if one is not provided')
    args = parser.parse_args()

    if args.instance is None:
        points, _ = make_blobs(n_samples=args.sample_size, centers=[
            [1, 1], [-1, -1], [1, -1]
        ], cluster_std=args.std)
        points = points.astype(np.float32)
        print('#sample: {}'.format(args.sample_size))
    else:
        points = load_instance(args.instance)

    bandwidth = args.bandwidth
    if bandwidth is None:
        bandwidth = estimate_bandwidth(points)

    print('Bandwidth: {}'.format(bandwidth))
    centers = meanshift(points, bandwidth)
    print("#clusters: {}".format(len(centers)))
    if len(centers) > 0 and len(centers[0]) == 2:
        show_clusters(centers, points, bandwidth)


if __name__ == '__main__':
    main()
