# source: https://devblogs.nvidia.com/building-cuda-applications-cmake/
cmake_minimum_required(VERSION 3.5 FATAL_ERROR)

IF(${CMAKE_VERSION} VERSION_LESS "3.8.0")
	message("Fallback mode: 3.5 compatibility")
	project(meanshift LANGUAGES CXX)

	find_package(CUDA REQUIRED)
	set(CUDA_PROPAGATE_HOST_FLAGS OFF)
	set(OLD_CMAKE TRUE)
ELSE()
	project(meanshift LANGUAGES CXX CUDA)
	set(OLD_CMAKE FALSE)
endif()

include_directories(inc)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -std=c++14")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")

set(CUDA_COMPUTE_VERSION "sm_30")
set(CMAKE_CUDA_FLAGS "-arch=${CUDA_COMPUTE_VERSION} -Xptxas -v --compiler-options -Wall,-Wextra,-std=c++14")
set(CMAKE_CUDA_FLAGS_DEBUG "${CMAKE_CUDA_FLAGS_DEBUG} -G")
set(CMAKE_CUDA_FLAGS_RELEASE "${CMAKE_CUDA_FLAGS_RELEASE} -lineinfo")
IF(OLD_CMAKE)
	# Just a big ugly workaround (issue: https://gitlab.kitware.com/cmake/cmake/issues/16411)
	string(REGEX REPLACE " " ";" CUDA_CLEANED_FLAGS ${CMAKE_CUDA_FLAGS})
	IF (CMAKE_BUILD_TYPE MATCHES Release)
		set(CUDA_NVCC_FLAGS_RELEASE "${CUDA_CLEANED_FLAGS};-std=c++14;--compiler-options;-fPIC")
	ELSEIF (CMAKE_BUILD_TYPE MATCHES Debug)
		set(CUDA_NVCC_FLAGS_DEBUG "${CUDA_CLEANED_FLAGS};-std=c++14;--compiler-options;-fPIC")
	ENDIF()
	set(CUDA_SEPARABLE_COMPILATION ON)
ENDIF()

IF (CMAKE_BUILD_TYPE MATCHES Debug)
	include(CTest)
	add_subdirectory(test)
ENDIF()

set(INCLUDE_FILES inc/meanshift.h inc/meanshift_internal.h inc/reduce_sum.h inc/vector.h inc/scalar_prod.cuh inc/norm.h inc/utils.h inc/random_generator.h)
set(SOURCE_FILES src/reduce_sum.cu src/vector.cpp src/scalar_prod.cu src/norm.cu src/random_generator.cpp src/meanshift.cpp)
IF (CPU_MODE)
	set(SOURCE_FILES ${SOURCE_FILES} src/meanshift_cpu.cpp)
ELSE()
	set(SOURCE_FILES ${SOURCE_FILES} src/meanshift_gpu.cu)
ENDIF()

find_package(PNG)
IF (PNG_FOUND)
	include_directories(${PNG_INCLUDE_DIR})
	set(ENABLE_executable ON)
ENDIF()

IF(OLD_CMAKE)
	cuda_add_library(meanshift STATIC ${INCLUDE_FILES} ${SOURCE_FILES})
ELSE()
	add_library(meanshift-obj OBJECT ${INCLUDE_FILES} ${SOURCE_FILES})
	set_property(TARGET meanshift-obj PROPERTY POSITION_INDEPENDENT_CODE ON)

	add_library(meanshift STATIC $<TARGET_OBJECTS:meanshift-obj>)
	add_library(meanshift-shared SHARED $<TARGET_OBJECTS:meanshift-obj>)

	# Request that meanshift be built with -std=c++14
	# As this is a public compile feature anything that links to meanshift will
	# also build with -std=c++14
	target_compile_features(meanshift PUBLIC cxx_std_14)

	# We need to explicitly state that we need all CUDA files in the meanshift
	# library to be built with -dc as the member functions could be called by other
	# libraries and executables
	set_target_properties(meanshift PROPERTIES CUDA_SEPARABLE_COMPILATION ON)
ENDIF()

# Executable
IF (ENABLE_executable)
	IF(OLD_CMAKE)
		cuda_add_executable(meanshift-bin src/main.cpp src/image/image-png.cpp inc/image/image.h)
	ELSE()
		add_executable(meanshift-bin src/main.cpp src/image/image-png.cpp inc/image/image.h)
	ENDIF()
	target_link_libraries(meanshift-bin meanshift ${PNG_LIBRARIES})
	set_target_properties(meanshift-bin PROPERTIES OUTPUT_NAME meanshift)
ENDIF()

# Executable for instances
IF(OLD_CMAKE)
	cuda_add_executable(meanshift-inst src/main_instances.cpp)
ELSE()
	add_executable(meanshift-inst src/main_instances.cpp)
ENDIF()
target_link_libraries(meanshift-inst meanshift ${CUDA_LIBRARIES})
